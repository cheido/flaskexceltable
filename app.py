from flask import Flask
import pandas as pd


app = Flask(__name__)

pd.set_option('display.width', 1000)
pd.set_option('colheader_justify', 'center')


@app.route("/")
def index():
    with open("links.xlsx"):
        df = pd.read_excel('links.xlsx', index_col=None, header=None,)
        df = df.fillna("")
        return df.to_html()


if __name__ == "__main__":
    app.run()
