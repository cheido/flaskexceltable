# FlaskExcelTable


## Name
FlaskExcelTable

## Description
Table Display of Links from Python Beginners and Intermediate Jeff.Pro Telegram Channels.
#
## Usage
Search for any keyword. Toggle through the found matches. The URLs are not hyperlinks, but copy URL and paste 
into your browser works! One column identifies Class videos. Other entries are from class participants. 
PostDate is the approximate date posted into our chats. Replit assignments refer to my posts, which may not all
work for everyone-- at least they are listed as a reminder of what assignments were done!

## Roadmap
I will continue to update with new links as we continue through our Intermediate Python course.

## Contributing
I am completely open to contributions on the app for this project. Formatting is minimal so far. I want to get a
nice Header at the top. I am a newbie with CSS and HTML!  I have no idea what to state about "requirements" for
contributing!! :)
TODO: make it pretty
TODO: make links clickable
TODO: Modify the method for accessing the excel data - programmatically rather that copy-sheet-save.as to project. 
Sounds like a database would be the way to go?
TODO: index.html needs to have standards applied; it works, but it needs help!


I will be happy to provide the original Excel workbook (one sheet is 'for_pycharm'), if anyone is interested.

...some documentation on how to get started....

...testing documentation...

## Authors and acknowledgment
I created this for my own use, but thought it would be appreciated by others as a source document, and would provide 
an opportunity to collaborate!

## License
Open source, for use by students in our classes. I have not created a license yet.

## Project status
I will continue to add links to the end of our Intermediate Python course.
